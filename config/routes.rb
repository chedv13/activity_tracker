Rails.application.routes.draw do
  ActiveAdmin.routes(self)

  controller :activity_entries do
    get 'activity_entries/projects_and_tasks' => :projects_and_tasks
    get 'activity_entries/tasks' => :tasks
  end

  # Init Devise for users
  devise_for :users

  # Init Active Admin routes
  devise_for :admin_users, ActiveAdmin::Devise.config

  get 'welcome/index'

  resources :absences, only: :index
  resources :activity_entries, only: :index
  resources :holidays, only: :index
  resources :projects, only: :index
  resources :tasks, only: :index

  root to: 'welcome#index'
end
