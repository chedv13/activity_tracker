json.array! @absences do |absence|
  json.date_from absence.DateFrom
  json.date_to absence.DateTo
end
