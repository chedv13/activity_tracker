json.array! @activity_entries do |date, activity_entries|
  json.date date.strftime('%Y-%m-%d')
  json.activity_entries activity_entries
end
