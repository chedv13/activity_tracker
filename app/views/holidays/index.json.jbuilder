json.array! @holidays do |holiday|
  json.date_from holiday.DateFrom
  json.date_to holiday.DateTo
end
