angular.module('activityTrackerApp.controllers', ['ui.bootstrap'])
    .constant('datepickerConfig', {
        formatDay: 'dd',
        formatMonth: 'MMMM',
        formatYear: 'yyyy',
        formatDayHeader: 'EEE',
        formatDayTitle: 'MMMM, yyyy',
        formatMonthTitle: 'yyyy',
        datepickerMode: 'day',
        minMode: 'day',
        maxMode: 'year',
        showWeeks: true,
        startingDay: 1,
        yearRange: 20,
        minDate: null,
        maxDate: null
    })
    .controller('MainController', ['$scope', '$http', '$attrs', '$parse', '$interpolate', '$timeout', '$log', '$document', 'dateFilter', 'datepickerConfig', function ($scope, $http, $attrs, $parse, $interpolate, $timeout, $log, $document,  dateFilter, datepickerConfig) {
        var duration = 1000; //milliseconds
        var offset = 30; //pixels; adjust for floating menu, context etc

        $scope.scroll = function(id) {
            var someElement = angular.element(document.getElementById(id));
            $document.scrollToElement(someElement, offset, duration);
        };



        var ngModelCtrl = { $setViewValue: angular.noop };

        // Configuration attributes
        angular.forEach(['formatDay', 'formatMonth', 'formatYear', 'formatDayHeader', 'formatDayTitle', 'formatMonthTitle',
            'minMode', 'maxMode', 'showWeeks', 'startingDay', 'yearRange'], function (key, index) {
            $scope[key] = angular.isDefined($attrs[key]) ? (index < 8 ? $interpolate($attrs[key])($scope.$parent) : $scope.$parent.$eval($attrs[key])) : datepickerConfig[key];
        });

        // Watchable attributes
        angular.forEach(['minDate', 'maxDate'], function (key) {
            if ($attrs[key]) {
                $scope.$parent.$watch($parse($attrs[key]), function (value) {
                    $scope[key] = value ? new Date(value) : null;
                    $scope.refreshView();
                });
            } else {
                $scope[key] = datepickerConfig[key] ? new Date(datepickerConfig[key]) : null;
            }
        });

        $scope.datepickerMode = $scope.datepickerMode || datepickerConfig.datepickerMode;
        $scope.uniqueId = 'datepicker-' + $scope.$id + '-' + Math.floor(Math.random() * 10000);
        $scope.activeDate = angular.isDefined($attrs.initDate) ? $scope.$parent.$eval($attrs.initDate) : new Date();
        $scope.step = { months: 1 };

        var
            compare = function (date1, date2) {
                return (new Date(date1.getFullYear(), date1.getMonth(), date1.getDate()) - new Date(date2.getFullYear(), date2.getMonth(), date2.getDate()) );
            },
            createDateObject = function (date, format) {
                var model = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : null;

                return {
                    date: date,
                    label: dateFilter(date, format),
                    selected: model && compare(date, model) === 0,
                    disabled: isDisabled(date),
                    current: compare(date, new Date()) === 0
                };
            },
            getDates = function (startDate, n) {
                var dates = new Array(n), current = new Date(startDate), i = 0;
                current.setHours(12);
                while (i < n) {
                    dates[i++] = new Date(current);
                    current.setDate(current.getDate() + 1);
                }
                return dates;
            },
            isDisabled = function (date) {
                return (($scope.minDate && compare(date, $scope.minDate) < 0) || ($scope.maxDate && compare(date, $scope.maxDate) > 0) ||
                    ($attrs.dateDisabled && $scope.dateDisabled({date: date, mode: $scope.datepickerMode})));
            },
            split = function (arr, size) {
                var arrays = [];

                while (arr.length > 0) {
                    arrays.push(arr.splice(0, size));
                }

                return arrays;
            };

        $scope.getActivityEntries = function () {
            var year = $scope.activeDate.getFullYear(),
                month = $scope.activeDate.getMonth(),
                firstDayOfMonth = new Date(year, month, 1),
                difference = $scope.startingDay - firstDayOfMonth.getDay(),
                numDisplayedFromPreviousMonth = (difference > 0) ? 7 - difference : -difference,
                firstDate = new Date(firstDayOfMonth);

            if (numDisplayedFromPreviousMonth > 0) {
                firstDate.setDate(1 - numDisplayedFromPreviousMonth);
            }

            // 42 is the number of days on a six-month calendar
            var days = getDates(firstDate, 42),
                formatFirstDate = moment(firstDate).format('YYYY-MM-DD'),
                formatLastDate = moment(_.last(days)).format('YYYY-MM-DD');

            for (var i = 0; i < 42; i++) {
                days[i] = angular.extend(createDateObject(days[i], $scope.formatDay), {
                    secondary: days[i].getMonth() !== month,
                    uid: $scope.uniqueId + '-' + i
                });
            }

            $scope.labels = new Array(7);
            for (var j = 0; j < 7; j++) {
                $scope.labels[j] = {
                    abbr: dateFilter(days[j].date, $scope.formatDayHeader),
                    full: dateFilter(days[j].date, 'EEEE')
                };
            }

            $scope.title = dateFilter($scope.activeDate, $scope.formatDayTitle);

            $http.get('/activity_entries.json?date_from=' + formatFirstDate + '&date_to=' + formatLastDate).
                success(function (dateActivityEntries) {
                    $http.get('/holidays.json?date_from=' + formatFirstDate + '&date_to=' + formatLastDate)
                        .success(function (holidayRanges) {
                            var holidays = [];

                            _.each(holidayRanges, function (holidayRange) {
                                var
                                    start = moment(holidayRange.date_from, "YYYY-MM-DD"),
                                    end = moment(holidayRange.date_to, "YYYY-MM-DD"),
                                    range = moment().range(start, end);

                                range.by('days', function (holiday) {
                                    holidays.push(holiday.format('YYYY-MM-DD'));
                                });
                            });


                            $http.get('/absences.json?date_from=' + formatFirstDate + '&date_to=' + formatLastDate)
                                .success(function (absenceRanges) {
                                    var
                                        absences = [],
                                        currentDate = moment().startOf('day');

                                    _.each(absenceRanges, function (absenceRange) {
                                        var
                                            start = moment(absenceRange.date_from, "YYYY-MM-DD"),
                                            end = moment(absenceRange.date_to, "YYYY-MM-DD"),
                                            range = moment().range(start, end);

                                        range.by('days', function (absence) {
                                            absences.push(absence.format('YYYY-MM-DD'));
                                        });
                                    });

                                    $scope.rows = _.map(split(days, 7), function (row) {
                                        return _.map(row, function (dt) {
                                            var
                                                momentDate = moment(dt.date),
                                                formatMomentDate = momentDate.format('YYYY-MM-DD'),
                                                dateActivityEntry = _.findWhere(dateActivityEntries, {date: formatMomentDate});

                                            dt.scrollToID = 'date_' + momentDate.format('YYYY_MM_DD');
                                            dt.activityEntries = dateActivityEntry.activity_entries;

                                            if (_.include([6, 7], momentDate.isoWeekday()) || _.include(holidays, formatMomentDate) || _.include(absences, formatMomentDate)) {
                                                dt.class = 'weekend';

                                                if (momentDate.month() != month) dt.class += ' another-month';

                                                if (_.isEmpty(dateActivityEntry.activity_entries)) {
                                                    if (momentDate > currentDate) {
                                                        dt.hoursSum = undefined;
                                                    } else {
                                                        dt.hoursSum = 0;
                                                    }
                                                } else {
                                                    var hoursSum = 0;

                                                    _.each(dateActivityEntry.activity_entries, function (activityEntries, projectName) {
                                                        hoursSum += _.reduce(activityEntries, function (memo, activity_entry) {
                                                            return memo + activity_entry.Hours;
                                                        }, 0);
                                                    });

                                                    dt.hoursSum = hoursSum;
                                                }
                                            } else {
                                                dt.class = 'weekday';

                                                if (momentDate.month() != month) dt.class += ' another-month';

                                                if (_.isEmpty(dateActivityEntry.activity_entries)) {
                                                    if (currentDate > momentDate) {
                                                        dt.hoursSum = 0;
                                                        dt.class += ' empty';
                                                    } else if (currentDate.format('YYYY-MM-DD') == momentDate.format('YYYY-MM-DD')) {
                                                        dt.hoursSum = 0;
                                                        dt.class += ' now'
                                                    } else {
                                                        dt.hoursSum = undefined;
                                                    }
                                                } else {
                                                    var hoursSum = 0;

                                                    _.each(dateActivityEntry.activity_entries, function (activityEntries, projectName) {
                                                        hoursSum += _.reduce(activityEntries, function (memo, activity_entry) {
                                                            return memo + activity_entry.Hours;
                                                        }, 0);
                                                    });

                                                    dt.hoursSum = hoursSum;
                                                }
                                            }

                                            return dt;
                                        })
                                    });

                                    $scope.actualDateActivityEntries = _.filter(_.flatten($scope.rows), function (dt) {
                                        return moment(dt.date).startOf('day') <= currentDate; // && (dt.class.indexOf('weekday') >= 0 || (dt.class.indexOf('weekend') >= 0 && dt.hoursSum));
                                    }).reverse();
                                })
                                .error(function (data, status, headers, config) {

                                });
                        })
                        .error(function (data, status, headers, config) {

                        });
                }).
                error(function (data, status, headers, config) {

                });
        };

        $scope.move = function (direction) {
            var year = $scope.activeDate.getFullYear() + direction * ($scope.step.years || 0),
                month = $scope.activeDate.getMonth() + direction * ($scope.step.months || 0);
            $scope.activeDate.setFullYear(year, month, 1);
            $scope.getActivityEntries();
        };

        $scope.isEmpty = function (arr) {
            return _.isEmpty(arr);
        };

        $scope.getMonth = function (date) {
            return moment(date).format('MMM');
        };

        $scope.isOvertime = function (activityEntry) {
            return _.include(['approvedOvertime', 'notApprovedOvertime'], activityEntry.Overtime);
        };

        $scope.isCurrentDate = function (dt) {
            return dt.class.indexOf('now') >= 0;
        };

        // Reduce for ActivityEntry array
        $scope.reduceActivityEntries = function (activityEntries) {
            return _.reduce(activityEntries, function (memo, activityEntry) {
                return memo + activityEntry.Hours;
            }, 0);
        };
    }])
    .controller('SidebarProjectsController', ['$scope', '$http', function ($scope, $http) {
        $http.get('/projects.json').success(function (projects) {
            $scope.sidebarProjects.activeProjects = _.groupBy(projects[0], function (project) {
                return project.customer_name;
            });

            $scope.sidebarProjects.inactiveProjects = _.groupBy(projects[1], function (project) {
                return project.customer_name;
            });
        });

//        $http.get('/projects.json?active=true').success(function (activeProjects) {
//            $scope.sidebarProjects.activeProjects = _.groupBy(activeProjects, function (project) {
//                return project.customer_name;
//            });
//        });
//
//        $http.get('/projects.json?active=false').success(function (inactiveProjects) {
//            $scope.sidebarProjects.inactiveProjects = _.groupBy(inactiveProjects, function (project) {
//                return project.customer_name;
//            });
//        });
    }])
    .controller('SearchController', ['$http', '$scope', function ($http, $scope) {
        // Config attributes
        $scope.search.selected = 0;
        $scope.search.queryForProjectsAndTasks = '';
        $scope.search.queryForTasks = '';
        $scope.search.selectedProject = undefined;
        $scope.search.selectedTask = undefined;
        $scope.search.hours = undefined;
        $scope.search.comment = undefined;

        // Watchers
        $scope.$watch('search.queryForProjectsAndTasks', function () {
            if ($scope.search.queryForProjectsAndTasks) {
                $http.get('/activity_entries/projects_and_tasks.json', {
                    params: {
                        query: $scope.search.queryForProjectsAndTasks
                    }
                }).success(function (data, status, headers, config) {
                    $scope.search.projectsAndTasks = data;
                    $scope.search.countItems = _.reduce(data, function (memo, activityEntries) {
                        return memo + activityEntries.length;
                    }, 0);
                }).error(function (data, status, headers, config) {
                    $scope.search.projectsAndTasks = [];
                    $scope.search.countItems = 0;
                });
            } else {
                $scope.search.projectsAndTasks = [];
                $scope.search.countItems = 0;
            }
        });

        $scope.$watch('search.queryForTasks', function () {
            if ($scope.search.queryForTasks) {
                $http.get('/activity_entries/tasks.json', {
                    params: {
                        project_id: $scope.search.selectedProject.projectID,
                        query: $scope.search.queryForTasks
                    }
                }).success(function (data, status, headers, config) {
                    $scope.search.tasks = data;
                }).error(function (data, status, headers, config) {
                    $scope.search.tasks = [];
                });
            } else {
                $scope.search.tasks = [];
            }
        });

        $scope.setActiveClass = function (index) {
            $scope.search.selected = index;
        };

        $scope.chooseItem = function (event, countProjects, projectsAndTasks) {
            if ($scope.search.selected > countProjects - 1) {
                var task = projectsAndTasks[1][$scope.search.selected - countProjects];

                $scope.search.selectedProject = { projectID: task.ProjectID, projectName: task.Name };
                $scope.search.selectedTask = { taskID: task.TaskID, taskTitle: task.Title };
            } else {
                var project = projectsAndTasks[0][$scope.search.selected];

                $scope.search.selectedProject = { projectID: project.ProjectID, projectName: project.Name };
            }

            event.preventDefault();
        };

        $scope.uptoItem = function (event, countItems) {
            if ($scope.search.selected == 0 && countItems > 1) {
                $scope.search.selected = countItems - 1;
            } else {
                $scope.search.selected -= 1;
            }

            event.preventDefault();
        };

        $scope.downtoItem = function (event, countItems) {
            if ($scope.search.selected == countItems - 1 && countItems > 1) {
                $scope.search.selected = 0;
            } else {
                $scope.search.selected += 1;
            }

            event.preventDefault();
        };

        $scope.removeProject = function () {
            $scope.search.hours = undefined;
            $scope.search.selectedTask = undefined;
            $scope.search.queryForTasks = '';
            $scope.search.selectedProject = undefined;
            $scope.search.queryForProjectsAndTasks = '';

        };

        $scope.removeTask = function () {
            $scope.search.hours = undefined;
            $scope.search.selectedTask = undefined;
            $scope.search.queryForTasks = '';
        };

        $scope.removeHours = function () {
            $scope.search.hours = undefined;
        };
    }])
;
