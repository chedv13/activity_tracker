require 'typhoeus/adapters/faraday'

class ActivityEntriesIndex < Chewy::Index
  settings analysis: {
      analyzer: {
          str_search_analyzer: {
              tokenizer: 'keyword',
              filter: ['lowercase']
          },
          str_index_analyzer: {
              tokenizer: 'keyword',
              filter: ['lowercase']
          }
      }
  }

  define_type ActivityEntry.for_import do
    field :comment, type: 'string'
    field :created_date, type: 'date'
    field :project_id, index_analyzer: 'str_index_analyzer', type: 'string', search_analyzer: 'str_search_analyzer'
    field :project_name, type: 'string'
    field :project_product_name, type: 'string'
    field :task_id, index_analyzer: 'str_index_analyzer', type: 'string', search_analyzer: 'str_search_analyzer'
    field :task_index, type: 'integer'
    field :task_title, type: 'string'
    field :user_id, index_analyzer: 'str_index_analyzer', type: 'string', search_analyzer: 'str_search_analyzer'
  end
end