require 'typhoeus/adapters/faraday'

class UsersIndex < Chewy::Index
  settings analysis: {
      analyzer: {
          user_id: {
              tokenizer: 'keyword',
              filter: ['lowercase']
          }
      }
  }

  define_type User do
    field :user_id, type: 'string', value: ->(user) { user.id }
  end
end