class ActivityEntry < ActiveRecord::Base
  include UUID

  update_index('activity_entries#activity_entry') { self }

  self.table_name = 'activityentry'

  belongs_to :user, foreign_key: 'UserID'
  belongs_to :task, foreign_key: 'TaskID'

  def self.by_user(user_id, date_range = nil)
    date_range ? where(UserID: user_id, Date: date_range) : where(UserID: user_id)
  end

  def self.by_user_for_period(user_id, date_range)
    activity_entries = where(UserID: user_id, Date: date_range).joins(:task).select('Comment, `activityentry`.`CreatedDate`, Date, Hours, Overtime, `tasks`.`Index`, `tasks`.`TaskID`, title').order('CreatedDate')

    date_range.inject({}) do |hash, date|
      hash[date] = activity_entries.select do |activity_entry|
        activity_entry.Date == date
      end.group_by do |activity_entry|
        activity_entry.task.project.Name
      end

      hash
    end
  end

  def self.for_import
    query = <<EOF
        `activityentry`.`ActivityEntryID`,
        `activityentry`.`Comment` AS comment,
        `activityentry`.`CreatedDate` AS created_date,
        `activityentry`.`TaskID` AS task_id,
        `activityentry`.`UserID` AS user_id,
        `projects`.`Name` AS project_name,
        `projects`.`ProductName` AS project_product_name,
        `projects`.`ProjectID` AS project_id,
        `tasks`.`Index` AS task_index,
        `tasks`.`Title` AS task_title
EOF

    select(query).joins('LEFT OUTER JOIN (tasks LEFT JOIN projects ON `tasks`.`ProjectID` = `projects`.`ProjectID`) ON `activityentry`.`TaskID` = `tasks`.`TaskID`')
  end

  # Get data from ElasticSearch
  def self.projects_and_tasks(user, query)
    projects_es = ActivityEntriesIndex.filter(and: [{term: {user_id: user.id}}, {regexp: {project_name: ".*#{query}.*"}}]).
        aggregations(project_id: {terms: {field: 'project_id', order: {max_created_date: 'desc'}}, aggs: {max_created_date: {max: {field: 'created_date'}}}})

    projects = projects_es.aggregations['project_id']['buckets'].take(10).map { |x| Project.find(x['key']) }

    tasks_es = ActivityEntriesIndex.filter(and: [{term: {user_id: user.id}}, {regexp: {task_title: ".*#{query}.*"}}]).
        aggregations(task_id: {terms: {field: 'task_id', order: {max_created_date: 'desc'}}, aggs: {max_created_date: {max: {field: 'created_date'}}}})

    tasks = tasks_es.aggregations['task_id']['buckets'].take(10).map { |x| Task.joins(:project).select('`tasks`.`Index`, TaskID, Title, `tasks`.`ProjectID`, Name').find(x['key']) }

    [projects, tasks]
  end

  def self.tasks(project_id, user, query)
    all_tasks_es = ActivityEntriesIndex.filter(and: [{term: {project_id: project_id}}, {regexp: {task_title: ".*#{query}.*"}}]).
        aggregations(task_id: {terms: {field: 'task_id', order: {max_created_date: 'desc'}}, aggs: {max_created_date: {max: {field: 'created_date'}}}})

    all_tasks = all_tasks_es.aggregations['task_id']['buckets'].map { |x| Task.find(x['key']) }

    assigned_tasks_es = ActivityEntriesIndex.filter(and: [{term: {project_id: project_id}}, {term: {user_id: user.id}}, {regexp: {task_title: ".*#{query}.*"}}]).
        aggregations(task_id: {terms: {field: 'task_id', order: {max_created_date: 'desc'}}, aggs: {max_created_date: {max: {field: 'created_date'}}}})

    assigned_tasks = assigned_tasks_es.aggregations['task_id']['buckets'].map { |x| Task.find(x['key']) }

    [assigned_tasks, all_tasks]
  end
end
