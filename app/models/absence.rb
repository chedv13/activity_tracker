class Absence < ActiveRecord::Base
  include UUID

  belongs_to :user, foreign_key: 'UserID'

  scope :by_user_for_period, ->(user_id, date_range) { where('UserID = ? AND (DateFrom IN (?) OR DateTo IN (?))', user_id, date_range, date_range) }
end
