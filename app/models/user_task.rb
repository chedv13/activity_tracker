class UserTask < ActiveRecord::Base
  self.table_name = 'usertask'

  belongs_to :user, foreign_key: 'UserID'
  belongs_to :task, foreign_key: 'TaskID'
end
