class Customer < ActiveRecord::Base
  include UUID

  self.table_name = 'customer'

  has_and_belongs_to_many :users, join_table: 'uservisiblecustomers', association_foreign_key: 'CustomerID'
  has_many :projects, foreign_key: 'CustomerID'
end
