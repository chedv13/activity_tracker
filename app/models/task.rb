class Task < ActiveRecord::Base
  include UUID

  update_index('activity_entities_index#task') { self }

  belongs_to :project, foreign_key: 'ProjectID'
  has_many :activity_entries, foreign_key: 'TaskID'
  has_many :user_tasks, foreign_key: 'TaskID'
  has_many :users, through: :user_tasks
end
