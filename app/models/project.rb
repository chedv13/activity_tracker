class Project < ActiveRecord::Base
  include UUID

  belongs_to :customer, foreign_key: 'CustomerID'
  has_many :tasks, foreign_key: 'ProjectID'

  def self.by_user(user)
    projects = user.tasks.select('DISTINCT(ProjectID)')
    projects_es = ActivityEntriesIndex.filter(and: [{term: {user_id: user.id}}]).
        aggregations(project_id: {terms: {field: 'project_id', order: {max_created_date: 'desc'}, size: projects.to_a.size},
                                  aggs: {max_created_date: {max: {field: 'created_date'}}}})
    separated_projects = [[], []]

    (projects.map(&:ProjectID) - projects_es.aggregations['project_id']['buckets'].map { |x| x['key'] }).each do |x|
      separated_projects[1] << Project.joins(:customer).select('`customer`.`Name` AS customer_name, `projects`.`Name`, ProjectID').find(x)
    end

    today = Date.today

    projects_es.aggregations['project_id']['buckets'].map do |x|
      date = Time.at(x['max_created_date']['value']/1000).to_date
      project = Project.joins(:customer).select('`customer`.`Name` AS customer_name, `projects`.`Name`, ProjectID').find(x['key'])

      if (today - 3.months..today).include?(date)
        separated_projects[0] << project
      else
        separated_projects[1] << project
      end
    end

    separated_projects
  end
end
