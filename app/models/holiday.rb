class Holiday < ActiveRecord::Base
  include UUID

  scope :for_period, ->(date_range) { where('DateFrom IN (?) OR DateTo IN (?)', date_range, date_range) }
end
