class User < ActiveRecord::Base
  include UUID

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :customers, join_table: 'uservisiblecustomers', association_foreign_key: 'CustomerID', foreign_key: 'UserID'
  has_many :absences, foreign_key: 'UserID'
  has_many :activity_entries, foreign_key: 'UserID'
  has_many :user_tasks, foreign_key: 'UserID'
  has_many :tasks, through: :user_tasks
end
