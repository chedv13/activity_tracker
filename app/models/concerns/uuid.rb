module UUID
  extend ActiveSupport::Concern

  included do
    before_create :generate_uuid

    def generate_uuid
      loop do
        id = SecureRandom.uuid

        unless self.class.exists?(id)
          self.id = id

          break
        end
      end
    end
  end
end