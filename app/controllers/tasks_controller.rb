class TasksController < ApplicationController
  def index
    ActivityEntry.by_user_for_period("c1dbd23d-2b74-42ba-8927-8a2d69ccc2e2", Date.parse("2014-07-28")..Date.parse("2014-09-07"))
  end
end
