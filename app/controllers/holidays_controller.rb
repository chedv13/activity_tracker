class HolidaysController < ApplicationController
  def index
    @holidays = Holiday.for_period(Date.parse(params[:date_from])..Date.parse(params[:date_to]))
  end
end
