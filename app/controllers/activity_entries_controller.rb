class ActivityEntriesController < ApplicationController
  respond_to :json

  def index
    @activity_entries = ActivityEntry.by_user_for_period(current_user.id, Date.parse(params[:date_from])..Date.parse(params[:date_to]))
  end

  def projects_and_tasks
    @projects_and_tasks = ActivityEntry.projects_and_tasks(current_user, params[:query])

    respond_with @projects_and_tasks
  end

  def tasks
    @tasks = ActivityEntry.tasks(params[:project_id], current_user, params[:query])

    respond_with @tasks
  end
end
