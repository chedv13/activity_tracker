class ProjectsController < ApplicationController
  respond_to :json

  def index
    @projects = Project.by_user(current_user)

    respond_with @projects
  end
end
