class AbsencesController < ApplicationController
  def index
    @absences = Absence.by_user_for_period(current_user.id, Date.parse(params[:date_from])..Date.parse(params[:date_to]))
  end
end
