class ChangeColumnsFromUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.rename :Email, :email
      t.rename :UserName, :username
    end
  end
end
