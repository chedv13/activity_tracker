# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140827075729) do

  create_table "absenceexports", primary_key: "ExportID", force: true do |t|
    t.string  "AbsenceID", limit: 40
    t.string  "ExternID",  limit: 40
    t.integer "State",     limit: 1
  end

  create_table "absences", primary_key: "AbsenceID", force: true do |t|
    t.date   "DateFrom"
    t.date   "DateTo"
    t.string "UserID",        limit: 40
    t.string "AbsenceTypeID", limit: 40
    t.text   "Comment"
  end

  add_index "absences", ["AbsenceTypeID"], name: "AbsenceTypeID", using: :btree
  add_index "absences", ["UserID"], name: "UserID", using: :btree

  create_table "absencetypes", primary_key: "AbsenceTypeID", force: true do |t|
    t.string "ColorName"
    t.string "Name"
  end

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "activityentry", primary_key: "ActivityEntryID", force: true do |t|
    t.string    "PrimaryEntryID",         limit: 40
    t.date      "Date"
    t.timestamp "CreatedDate"
    t.float     "Hours",                  limit: 53
    t.text      "Comment",                                                   null: false
    t.string    "ReviewedEntryID",        limit: 40
    t.string    "UserID",                 limit: 40
    t.string    "UserProjectRoleGroupID", limit: 40
    t.string    "TaskID",                 limit: 40
    t.boolean   "IsDeleted"
    t.string    "ActivityEntry_Type",                                        null: false
    t.string    "SubmitUserID",           limit: 40
    t.string    "Overtime",               limit: 40, default: "notOvertime", null: false
  end

  add_index "activityentry", ["CreatedDate", "UserID"], name: "CreatedDateUserIndex", using: :btree
  add_index "activityentry", ["CreatedDate"], name: "CreatedDateIndex", using: :btree
  add_index "activityentry", ["Date"], name: "Date", using: :btree
  add_index "activityentry", ["PrimaryEntryID"], name: "PrimaryEntryID", using: :btree
  add_index "activityentry", ["ReviewedEntryID"], name: "ReviewedEntryID", using: :btree
  add_index "activityentry", ["TaskID"], name: "TaskID", using: :btree
  add_index "activityentry", ["UserID"], name: "UserID", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "auditrecord", primary_key: "AuditRecordID", force: true do |t|
    t.string    "ObjectName"
    t.string    "ObjectId"
    t.string    "ObjectAuditableName"
    t.integer   "AuditLogType"
    t.string    "UserId",              limit: 40
    t.string    "FieldName"
    t.timestamp "Date"
    t.string    "OldData"
    t.string    "NewData"
  end

  create_table "changerequest", primary_key: "RequestID", force: true do |t|
    t.string  "ProjectID",         limit: 40
    t.string  "ReadableID"
    t.string  "Title"
    t.date    "SubmittedDate"
    t.string  "Cost"
    t.string  "ScheduleImpact"
    t.integer "StatusID"
    t.string  "Version"
    t.string  "BillingStatus"
    t.string  "Reference"
    t.string  "DecisionReference"
    t.text    "Comment"
  end

  create_table "changerequestestimatesandcategories", id: false, force: true do |t|
    t.integer "ChangeRequestID",            null: false
    t.float   "Estimate",        limit: 24
    t.string  "CategoryID",      limit: 40, null: false
  end

  create_table "changerequeststatus", primary_key: "StatusID", force: true do |t|
    t.string "Name"
    t.string "Tag"
  end

  create_table "conditions", id: false, force: true do |t|
    t.string  "customview_id", limit: 40,         null: false
    t.string  "Field",                            null: false
    t.boolean "Not",                              null: false
    t.string  "Term",                             null: false
    t.binary  "Value",         limit: 2147483647, null: false
    t.string  "AndOr",                            null: false
  end

  add_index "conditions", ["customview_id"], name: "customview_id", using: :btree

  create_table "customer", primary_key: "CustomerID", force: true do |t|
    t.string   "Name"
    t.string   "Alias"
    t.string   "ReportName"
    t.text     "Description"
    t.datetime "LastActivity"
    t.integer  "CustomerShortID", null: false
  end

  add_index "customer", ["CustomerShortID"], name: "CustomerShortID", unique: true, using: :btree

  create_table "customerbillingcategories", primary_key: "BillingCategoryID", force: true do |t|
    t.string  "Name"
    t.integer "Order"
  end

  create_table "customerbillingcategoryconformity", id: false, force: true do |t|
    t.string  "BillingCategoryID", limit: 40
    t.string  "TaskCategoryID",    limit: 40
    t.string  "ProjectRoleID",     limit: 40
    t.string  "EmployeeTypeID",    limit: 40
    t.integer "Priority"
  end

  create_table "customerbillingcategoryconformitylnb", id: false, force: true do |t|
    t.string  "BillingCategoryID", limit: 40
    t.string  "TaskCategoryID",    limit: 40
    t.string  "ProjectRoleID",     limit: 40
    t.string  "EmployeeTypeID",    limit: 40
    t.integer "Priority"
  end

  create_table "customerbillingentries", primary_key: "BillingEntryID", force: true do |t|
    t.datetime "Date"
    t.string   "CustomerID", limit: 40
    t.float    "Billing",    limit: 24
    t.string   "CategoryID", limit: 40
  end

  create_table "customerpaymententries", primary_key: "PaymentEntryID", force: true do |t|
    t.string "CustomerID",  limit: 40
    t.date   "PeriodStart"
    t.date   "PeriodEnd"
    t.float  "Sum",         limit: 24
    t.float  "Invoice",     limit: 24
  end

  create_table "customfield", primary_key: "ID", force: true do |t|
    t.string "CustomField_Type",                     null: false
    t.binary "Value",             limit: 2147483647
    t.string "TaskID",            limit: 40
    t.string "CustomFieldTypeID", limit: 40
    t.string "ActivityEntryID",   limit: 40
    t.string "ProjectID",         limit: 40
  end

  add_index "customfield", ["ActivityEntryID"], name: "ActivityEntryID", using: :btree
  add_index "customfield", ["CustomFieldTypeID"], name: "CustomFieldTypeID", using: :btree
  add_index "customfield", ["ProjectID"], name: "ProjectID", using: :btree
  add_index "customfield", ["TaskID"], name: "TaskID", using: :btree

  create_table "customfieldpositions", id: false, force: true do |t|
    t.string  "CustomViewID",      limit: 40, null: false
    t.integer "Position"
    t.string  "CustomFieldTypeID", limit: 40
  end

  add_index "customfieldpositions", ["CustomFieldTypeID"], name: "CustomFieldTypeID", using: :btree
  add_index "customfieldpositions", ["CustomViewID"], name: "CustomViewID", using: :btree

  create_table "customfieldprototype", primary_key: "ID", force: true do |t|
    t.string  "CustomFieldPrototype_TYPE",                                    null: false
    t.string  "Name"
    t.binary  "PossibleValues",            limit: 2147483647
    t.boolean "IsEditable",                                   default: false, null: false
  end

  create_table "customfieldtype", primary_key: "CustomFieldTypeID", force: true do |t|
    t.string  "ProjectID",              limit: 40,         null: false
    t.string  "CustomFieldPrototypeID", limit: 40,         null: false
    t.string  "Name",                                      null: false
    t.string  "CustomFieldType_Type",                      null: false
    t.binary  "DefaultValue",           limit: 2147483647
    t.boolean "IsRequired"
    t.boolean "IsDeleted"
  end

  add_index "customfieldtype", ["CustomFieldPrototypeID"], name: "CustomFieldPrototypeID", using: :btree
  add_index "customfieldtype", ["ProjectID"], name: "ProjectID", using: :btree

  create_table "customview", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.integer "ViewOrder"
    t.boolean "IsDefault"
    t.boolean "EnabledForCustomer"
    t.binary  "FieldsPositions",    limit: 2147483647
    t.string  "ProjectID",          limit: 40
    t.string  "CustomerID",         limit: 40
    t.string  "CustomView_TYPE",                       null: false
  end

  add_index "customview", ["CustomerID"], name: "CustomerID", using: :btree
  add_index "customview", ["ProjectID"], name: "ProjectID", using: :btree

  create_table "customviewbrowsingoptions", id: false, force: true do |t|
    t.string  "customview_id", limit: 40, null: false
    t.boolean "IsExpanded"
    t.boolean "IsSelected"
    t.string  "PageName"
    t.string  "UserID",        limit: 40
  end

  add_index "customviewbrowsingoptions", ["UserID"], name: "UserID", using: :btree
  add_index "customviewbrowsingoptions", ["customview_id"], name: "customview_id", using: :btree

  create_table "dcbaserole", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description", limit: 45
    t.integer "Order"
  end

  create_table "dcemployeetype", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description"
    t.integer "Order"
    t.integer "AssociatedPositionID"
  end

  create_table "dcpriority", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description"
    t.integer "Order"
  end

  create_table "dcprojectstate", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description"
    t.integer "Order"
  end

  create_table "dcrequesttype", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description"
    t.integer "Order"
  end

  create_table "dcresolution", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description"
    t.integer "Order"
  end

  create_table "dcworkcategory", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description"
    t.integer "Order"
  end

  create_table "dcworkflowstatus", primary_key: "ID", force: true do |t|
    t.string  "Name"
    t.string  "Description"
    t.integer "Order"
  end

  create_table "discountentries", primary_key: "DiscountEntryID", force: true do |t|
    t.date   "DateFrom"
    t.date   "DateTo"
    t.float  "Rate",      limit: 53
    t.text   "Comment"
    t.string "UserID",    limit: 40
    t.string "ProjectID", limit: 40
  end

  create_table "employeepositionchanges", primary_key: "ID", force: true do |t|
    t.string   "UserID",        limit: 40
    t.datetime "Date"
    t.integer  "OldPositionID"
    t.integer  "NewPositionID"
  end

  create_table "employeepositionrates", id: false, force: true do |t|
    t.integer "EmployeePositionID",            null: false
    t.float   "Rate",               limit: 24
    t.date    "Date"
  end

  create_table "employeepositions", primary_key: "PositionID", force: true do |t|
    t.string  "Name"
    t.integer "Order"
  end

  create_table "eventmessage", primary_key: "ID", force: true do |t|
    t.datetime "CreationDate"
    t.datetime "ScheduledDate"
    t.binary   "ATEventInfo",      limit: 2147483647
    t.integer  "EventMessageType",                    null: false
  end

  create_table "exceptionslog", primary_key: "ID", force: true do |t|
    t.binary   "ExceptionBody",  limit: 2147483647, null: false
    t.datetime "Date",                              null: false
    t.binary   "AdditionalInfo", limit: 2147483647
    t.text     "Message",                           null: false
  end

  create_table "fieldsorders", id: false, force: true do |t|
    t.string  "customview_id", limit: 40, null: false
    t.string  "Field",                    null: false
    t.integer "Order",                    null: false
  end

  add_index "fieldsorders", ["customview_id"], name: "customview_id", using: :btree

  create_table "globalrolegroups", primary_key: "GlobalRoleGroupID", force: true do |t|
    t.string "Name",        null: false
    t.string "Description"
  end

  add_index "globalrolegroups", ["Name"], name: "Name", unique: true, using: :btree

  create_table "globalroleprototypes", primary_key: "GlobalRolePrototypeID", force: true do |t|
    t.string "Name",        null: false
    t.string "Description"
    t.string "Category",    null: false
  end

  create_table "globalroles", primary_key: "GlobalRoleID", force: true do |t|
    t.boolean "Checked"
    t.string  "GlobalRolePrototypeID", limit: 40
    t.string  "GlobalRoleGroupID",     limit: 40
    t.string  "UserID",                limit: 40
  end

  add_index "globalroles", ["GlobalRoleGroupID"], name: "GlobalRoleGroupID", using: :btree
  add_index "globalroles", ["GlobalRolePrototypeID"], name: "GlobalRolePrototypeID", using: :btree
  add_index "globalroles", ["UserID"], name: "UserID", using: :btree

  create_table "groupofroles", primary_key: "GroupOfRolesID", force: true do |t|
    t.string "Name"
  end

  create_table "holidays", primary_key: "HolidayID", force: true do |t|
    t.date    "DateFrom"
    t.date    "DateTo"
    t.boolean "IsAdditionalWorkDay"
    t.text    "Comment"
  end

  create_table "indirectcostpayments", id: false, force: true do |t|
    t.integer "TypeID"
    t.float   "Sum",       limit: 24
    t.date    "MonthDate"
  end

  create_table "indirectcosttypes", primary_key: "TypeID", force: true do |t|
    t.string  "Name"
    t.string  "Tag"
    t.integer "Order", null: false
  end

  create_table "loadandbillingfilterlist", force: true do |t|
    t.string "employeeId",   limit: 45
    t.string "employeeType", limit: 45
    t.string "employeeRole", limit: 45
    t.string "guid",         limit: 45
  end

  create_table "mailgroups", primary_key: "MailGroupID", force: true do |t|
    t.string "Name"
    t.string "CustomerID",      limit: 40
    t.string "MailGroupTypeID", limit: 40
  end

  add_index "mailgroups", ["CustomerID"], name: "ProjectID", using: :btree
  add_index "mailgroups", ["MailGroupTypeID"], name: "FK_MailGroupTypeID", using: :btree

  create_table "mailgrouptypes", primary_key: "MailGroupID", force: true do |t|
    t.string "Description"
  end

  create_table "montheditstates", primary_key: "ID", force: true do |t|
    t.date    "Date"
    t.boolean "Editable"
  end

  create_table "poisoneventmessage", primary_key: "ID", force: true do |t|
    t.binary "ATEventInfo", limit: 2147483647
  end

  create_table "projectcostreport", primary_key: "ID", force: true do |t|
    t.datetime "LastCalculation"
    t.date     "StartMonthDate"
    t.date     "EndMonthDate"
  end

  create_table "projectcostreportcolumns", primary_key: "ColumnID", force: true do |t|
    t.string  "Name"
    t.string  "Tag"
    t.integer "Order"
  end

  create_table "projectcostreportentries", primary_key: "ID", force: true do |t|
    t.integer "ProjectCostReportID"
    t.string  "Project",             limit: 100
    t.string  "ProjectID",           limit: 40
    t.float   "Income",              limit: 53
    t.float   "DirectExpenses",      limit: 53
    t.float   "IndirectExpenses",    limit: 53
    t.float   "Profit",              limit: 53
    t.integer "ParentID"
    t.boolean "Leaf"
  end

  add_index "projectcostreportentries", ["ProjectCostReportID"], name: "ProjectCostReportID", using: :btree

  create_table "projectcostreportmonthentries", primary_key: "EntryID", force: true do |t|
    t.date     "MonthDate"
    t.datetime "LastCalculationTime"
  end

  create_table "projectprogressentries", primary_key: "ProgressEntryID", force: true do |t|
    t.datetime "Date"
    t.float    "Progress",  limit: 24
    t.integer  "RequestID"
  end

  add_index "projectprogressentries", ["RequestID"], name: "RequestID", using: :btree

  create_table "projectrolegroups", primary_key: "ProjectRoleGroupID", force: true do |t|
    t.string  "Name",                                null: false
    t.string  "Description"
    t.string  "BaseRoleID",   limit: 40
    t.integer "Order",                   default: 0
    t.string  "DefaultAlias"
  end

  add_index "projectrolegroups", ["BaseRoleID"], name: "FK_projectrolegroups_to_baserole", using: :btree
  add_index "projectrolegroups", ["Name"], name: "Name", unique: true, using: :btree

  create_table "projectrolegroupuserpositionmap", id: false, force: true do |t|
    t.string  "ProjectRoleGroupID", limit: 40
    t.integer "EmployeePositionID"
  end

  create_table "projectroleprototypes", primary_key: "ProjectRolePrototypeID", force: true do |t|
    t.string "Name",        null: false
    t.string "Description"
    t.string "Category",    null: false
  end

  create_table "projectroles", primary_key: "ProjectRoleID", force: true do |t|
    t.boolean "Checked"
    t.string  "ProjectRolePrototypeID",    limit: 40
    t.string  "UserInProjectAssignmentID", limit: 40
    t.string  "ProjectRoleGroupID",        limit: 40
  end

  add_index "projectroles", ["ProjectRoleGroupID"], name: "ProjectRoleGroupID", using: :btree
  add_index "projectroles", ["ProjectRolePrototypeID"], name: "ProjectRolePrototypeID", using: :btree
  add_index "projectroles", ["UserInProjectAssignmentID"], name: "UserInProjectAssignmentID", using: :btree

  create_table "projects", primary_key: "ProjectID", force: true do |t|
    t.string    "Name"
    t.string    "ProductName"
    t.string    "Alias"
    t.string    "ProjectStateID",           limit: 40
    t.string    "CustomerID",               limit: 40
    t.timestamp "CreatedDate"
    t.timestamp "LastUpdatedDate"
    t.date      "StartDate"
    t.date      "FinishDate"
    t.boolean   "IsCommentRequired"
    t.boolean   "NewTaskIsPublicByDefault",            default: false, null: false
    t.string    "ManagerID",                limit: 40
    t.string    "DefaultMailGroup",         limit: 40
    t.string    "ProjectType",              limit: 10
    t.string    "PMTask",                   limit: 40
    t.string    "JiraSyncKey",              limit: 40
    t.boolean   "IsJiraScheduleSync",                  default: false
    t.boolean   "IsBillingActive",                     default: false
    t.boolean   "IsReduceNames",                       default: true
  end

  add_index "projects", ["CustomerID"], name: "CustomerID", using: :btree
  add_index "projects", ["DefaultMailGroup"], name: "FK_DefaultMailGroup", using: :btree
  add_index "projects", ["ManagerID"], name: "ManagerID", using: :btree
  add_index "projects", ["PMTask"], name: "FK_PMTask", using: :btree
  add_index "projects", ["ProjectStateID"], name: "ProjectStateID", using: :btree

  create_table "queueeventmessage", primary_key: "ID", force: true do |t|
    t.binary "ATEventInfo", limit: 2147483647
  end

  create_table "rolecategories", primary_key: "RoleCategoryID", force: true do |t|
    t.string "CategoryName"
  end

  create_table "roles", primary_key: "RoleID", force: true do |t|
    t.string "RoleName",                  null: false
    t.string "RoleCategoryID", limit: 40
  end

  add_index "roles", ["RoleCategoryID"], name: "RoleCategoryID", using: :btree

  create_table "rolesingroups", primary_key: "RoleInGroupID", force: true do |t|
    t.string  "GroupOfRolesID", limit: 40
    t.string  "RoleID",         limit: 40
    t.boolean "Checked"
  end

  add_index "rolesingroups", ["GroupOfRolesID"], name: "GroupOfRolesID", using: :btree
  add_index "rolesingroups", ["RoleID"], name: "RoleID", using: :btree

  create_table "sentmails", primary_key: "MailID", force: true do |t|
    t.datetime "DateSent"
    t.string   "Recipients",   limit: 500
    t.string   "MailType",     limit: 100
    t.text     "ErrorMessage"
  end

  create_table "systemvariable", primary_key: "ID", force: true do |t|
    t.string "Name",  limit: 128
    t.string "Value"
  end

  create_table "taskattachments", primary_key: "ID", force: true do |t|
    t.binary "Attachment", limit: 2147483647
    t.text   "Name"
    t.string "TaskID",     limit: 40
  end

  add_index "taskattachments", ["TaskID"], name: "TaskID", using: :btree

  create_table "taskimports", primary_key: "ImportID", force: true do |t|
    t.string   "TaskID",         limit: 40
    t.string   "ExternID",       limit: 40
    t.datetime "LastUpdateTime"
  end

  create_table "taskmessage", primary_key: "ID", force: true do |t|
    t.text      "Text"
    t.string    "CreatedBy"
    t.timestamp "CreatedDate"
    t.string    "UpdatedBy"
    t.timestamp "UpdatedDate"
    t.boolean   "IsPublic"
    t.string    "TaskID",      limit: 40
    t.string    "UserID",      limit: 40
  end

  add_index "taskmessage", ["TaskID"], name: "TaskID", using: :btree
  add_index "taskmessage", ["UserID"], name: "UserID", using: :btree

  create_table "taskmessageattachments", primary_key: "ID", force: true do |t|
    t.binary "Attachment",    limit: 2147483647
    t.text   "Name"
    t.string "TaskMessageID", limit: 40
  end

  add_index "taskmessageattachments", ["TaskMessageID"], name: "TaskMessageID", using: :btree

  create_table "tasks", primary_key: "TaskID", force: true do |t|
    t.string    "Title"
    t.timestamp "CreatedDate"
    t.timestamp "UpdatedDate"
    t.date      "FinishDate"
    t.date      "CustomerDeadline"
    t.text      "Description"
    t.float     "EstimateHours",            limit: 53, default: 0.0, null: false
    t.float     "Completed",                limit: 53
    t.date      "EstimatedDevelopmentDate"
    t.boolean   "IsCommentRequired"
    t.integer   "Index"
    t.boolean   "IsPublic"
    t.string    "ResolutionID",             limit: 40
    t.string    "ProjectID",                limit: 40
    t.string    "CategoryID",               limit: 40
    t.string    "PriorityID",               limit: 40
    t.string    "RequestTypeID",            limit: 40
    t.string    "MailGroupID",              limit: 40
    t.string    "RequestedVersionID",       limit: 40
    t.string    "WorkflowStatusID",         limit: 40
    t.string    "CreatedByID",              limit: 40
    t.string    "UpdatedByID",              limit: 40
    t.string    "FirstMessageID",           limit: 40
  end

  add_index "tasks", ["CategoryID"], name: "CategoryID", using: :btree
  add_index "tasks", ["CreatedByID"], name: "CreatedByID", using: :btree
  add_index "tasks", ["FirstMessageID"], name: "FirstMessageID", unique: true, using: :btree
  add_index "tasks", ["FirstMessageID"], name: "FirstMessageID_2", using: :btree
  add_index "tasks", ["MailGroupID"], name: "MailGroupID", using: :btree
  add_index "tasks", ["PriorityID"], name: "PriorityID", using: :btree
  add_index "tasks", ["ProjectID"], name: "ProjectID", using: :btree
  add_index "tasks", ["RequestTypeID"], name: "RequestTypeID", using: :btree
  add_index "tasks", ["RequestedVersionID"], name: "RequestedVersionID", using: :btree
  add_index "tasks", ["ResolutionID"], name: "ResolutionID", using: :btree
  add_index "tasks", ["UpdatedByID"], name: "UpdatedByID", using: :btree
  add_index "tasks", ["WorkflowStatusID"], name: "WorkflowStatusID", using: :btree

  create_table "userinprojectassignments", primary_key: "UserInProjectAssignmentID", force: true do |t|
    t.string  "UserID",             limit: 40,                 null: false
    t.string  "ProjectID",          limit: 40,                 null: false
    t.string  "Alias"
    t.string  "ProjectRoleGroupID", limit: 40,                 null: false
    t.string  "DefaultVersionID",   limit: 40
    t.string  "DefaultTaskID",      limit: 40
    t.boolean "IsDisabled",                    default: false, null: false
    t.boolean "IsActive",                      default: true,  null: false
    t.integer "IsVisible",          limit: 1,  default: 1,     null: false
  end

  add_index "userinprojectassignments", ["DefaultTaskID"], name: "DefaultTaskID", using: :btree
  add_index "userinprojectassignments", ["DefaultVersionID"], name: "DefaultVersionID", using: :btree
  add_index "userinprojectassignments", ["ProjectID"], name: "ProjectID", using: :btree
  add_index "userinprojectassignments", ["ProjectRoleGroupID"], name: "ProjectRoleGroupID", using: :btree
  add_index "userinprojectassignments", ["UserID"], name: "UserID", using: :btree

  create_table "userinversion", primary_key: "UserInVersionID", force: true do |t|
    t.string  "UserID",        limit: 40,             null: false
    t.string  "VersionID",     limit: 40,             null: false
    t.string  "DefaultTaskID", limit: 40
    t.integer "IsVisible",     limit: 1,  default: 1, null: false
  end

  add_index "userinversion", ["DefaultTaskID"], name: "DefaultTaskID", using: :btree
  add_index "userinversion", ["UserID"], name: "UserID", using: :btree
  add_index "userinversion", ["VersionID"], name: "VersionID", using: :btree

  create_table "userinvisiblelistprojects", id: false, force: true do |t|
    t.string "UserID",    limit: 40
    t.string "ProjectID", limit: 40
  end

  create_table "users", primary_key: "UserID", force: true do |t|
    t.string    "FirstName"
    t.string    "LastName"
    t.string    "FullNameRus"
    t.integer   "TableNumber"
    t.string    "email"
    t.integer   "Rate"
    t.string    "username",                                                       null: false
    t.string    "Comment"
    t.string    "Password",                                                       null: false
    t.string    "PasswordQuestion"
    t.string    "PasswordAnswer"
    t.boolean   "IsApproved",                                                     null: false
    t.timestamp "LastActivityDate"
    t.timestamp "LastLoginDate"
    t.timestamp "LastPasswordChangedDate"
    t.timestamp "CreateDate"
    t.boolean   "IsOnline",                                                       null: false
    t.boolean   "IsLockedOut",                                                    null: false
    t.timestamp "LastLockoutDate"
    t.integer   "FailedPasswordAttemptCount"
    t.timestamp "FailedPasswordAttemptWindowStart"
    t.integer   "FailedPasswordAnswerAttemptCount"
    t.timestamp "FailedPasswordAnswerAttemptWindowStart"
    t.string    "DefaultProjectID",                       limit: 40
    t.string    "EmployeeTypeID",                         limit: 40
    t.integer   "EmployeePositionID"
    t.string    "GlobalRoleGroupID",                      limit: 40,              null: false
    t.string    "ResourceManagerID",                      limit: 40
    t.string    "ResourceRootID",                         limit: 40
    t.boolean   "IsDisabled",                                                     null: false
    t.datetime  "DisableTime"
    t.boolean   "SendMissingEntriesReport",                                       null: false
    t.integer   "MissingEntriesPeriod",                   limit: 2,               null: false
    t.integer   "TimeZoneOffset",                         limit: 8
    t.string    "encrypted_password",                                default: "", null: false
    t.string    "reset_password_token"
    t.datetime  "reset_password_sent_at"
    t.datetime  "remember_created_at"
    t.integer   "sign_in_count",                                     default: 0,  null: false
    t.datetime  "current_sign_in_at"
    t.datetime  "last_sign_in_at"
    t.string    "current_sign_in_ip"
    t.string    "last_sign_in_ip"
    t.datetime  "created_at"
    t.datetime  "updated_at"
  end

  add_index "users", ["DefaultProjectID"], name: "DefaultProjectID", using: :btree
  add_index "users", ["EmployeeTypeID"], name: "EmployeeTypeID", using: :btree
  add_index "users", ["GlobalRoleGroupID"], name: "GlobalRoleGroupID", using: :btree
  add_index "users", ["ResourceManagerID"], name: "ResourceManagerID", using: :btree
  add_index "users", ["ResourceRootID"], name: "ResourceRootID", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "usersinmailgroups", id: false, force: true do |t|
    t.string "MailGroupID", limit: 40, null: false
    t.string "UserID",      limit: 40, null: false
  end

  add_index "usersinmailgroups", ["MailGroupID"], name: "MailGroupID", using: :btree
  add_index "usersinmailgroups", ["UserID"], name: "UserID", using: :btree

  create_table "usersinprojectgroups", primary_key: "UserInProjectGroupID", force: true do |t|
    t.string "GroupOfRolesID", limit: 40
    t.string "UserID",         limit: 40
    t.string "ProjectID",      limit: 40
  end

  add_index "usersinprojectgroups", ["GroupOfRolesID"], name: "GroupOfRolesID", using: :btree
  add_index "usersinprojectgroups", ["ProjectID"], name: "ProjectID", using: :btree
  add_index "usersinprojectgroups", ["UserID"], name: "UserID", using: :btree

  create_table "usersinroles", primary_key: "UserInRoleID", force: true do |t|
    t.string  "UserID",    limit: 40
    t.string  "RoleID",    limit: 40
    t.string  "ProjectID", limit: 40
    t.boolean "Checked"
  end

  add_index "usersinroles", ["ProjectID"], name: "ProjectID", using: :btree
  add_index "usersinroles", ["RoleID"], name: "RoleID", using: :btree
  add_index "usersinroles", ["UserID"], name: "UserID", using: :btree

  create_table "usertask", id: false, force: true do |t|
    t.string  "UserID",     limit: 40,                 null: false
    t.boolean "IsHidden",              default: false, null: false
    t.string  "TaskID",     limit: 40,                 null: false
    t.string  "UserTaskID", limit: 40,                 null: false
  end

  add_index "usertask", ["TaskID"], name: "TaskID", using: :btree
  add_index "usertask", ["UserID"], name: "UserID", using: :btree

  create_table "uservisiblecostprojects", id: false, force: true do |t|
    t.string "UserID",    limit: 40
    t.string "ProjectID", limit: 40
  end

  create_table "uservisiblecustomers", id: false, force: true do |t|
    t.string "UserID",     limit: 40
    t.string "CustomerID", limit: 40
  end

  create_table "version", primary_key: "VersionID", force: true do |t|
    t.string    "Name"
    t.string    "Description"
    t.string    "ProjectID",   limit: 40
    t.timestamp "CreatedDate"
  end

  add_index "version", ["ProjectID"], name: "ProjectID", using: :btree

end
